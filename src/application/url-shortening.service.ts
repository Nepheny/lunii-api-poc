import { UrlPort } from '../domain/ports/url-port';
import UrlToCreate from '../domain/models/url-to-create';
import Url from '../domain/models/url';

export default class UrlShorteningService {
  constructor(private urlPort: UrlPort) {}

  async saveShortUrl(shortUrlToCreate: UrlToCreate): Promise<Url> {
    const url = new Url(shortUrlToCreate.originalUrl, shortUrlToCreate.shortUrl);
    await this.urlPort.save(url);
    return url;
  }

  async getOriginalUrl(shortUrl: string): Promise<Url | null> {
    const url = await this.urlPort.findByShortUrl(shortUrl);
    await this.urlPort.updateNbVisited(shortUrl);
    return url;
  }

  async getAll(): Promise<Url[]> {
    return this.urlPort.findAll();
  }
}
