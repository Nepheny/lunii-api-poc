import StringAsserter from './string.asserter';
import MissingMandatoryValueException from '../exceptions/missing-mandatory-value.exception';
import NumberAsserter from './number.asserter';

export default class Assert {
  public static notNull(field: string, input: unknown): void {
    if (input === undefined || input === null) {
      throw MissingMandatoryValueException.forNullValue(field);
    }
  }

  static string(field: string, input: unknown): StringAsserter {
    this.notNull(field, input);
    return new StringAsserter(field, input);
  }

  static number(field: string, input: unknown): NumberAsserter {
    this.notNull(field, input);
    return new NumberAsserter(field, input);
  }
}
