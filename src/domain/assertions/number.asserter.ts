import UnexpectedValueException from '../exceptions/unexpected-value.exception';
import NumberValueTooLowException from '../exceptions/number-value-too-low.exception';

export default class NumberAsserter {
  private readonly field: string;

  private readonly input: number;

  constructor(field: string, input: unknown) {
    if (typeof input !== 'number') {
      throw UnexpectedValueException.forExpectedFormat(field, input, 'number');
    }
    this.field = field;
    this.input = input;
  }

  public integer(): NumberAsserter {
    if (!Number.isSafeInteger(this.input)) {
      throw UnexpectedValueException.forExpectedFormat(this.field, this.input, 'integer');
    }
    return this;
  }

  public min(minValue: number): NumberAsserter {
    if (this.input < minValue) {
      throw new NumberValueTooLowException({ field: this.field, value: this.input, minValue });
    }
    return this;
  }
}
