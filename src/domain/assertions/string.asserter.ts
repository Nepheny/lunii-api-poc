import validator from 'validator';
import UnexpectedValueException from '../exceptions/unexpected-value.exception';
import MissingMandatoryValueException from '../exceptions/missing-mandatory-value.exception';

export default class StringAsserter {
  private readonly field: string;

  private readonly input: string;

  constructor(field: string, input: unknown) {
    if (typeof input !== 'string') {
      throw UnexpectedValueException.forExpectedFormat(field, input, 'string');
    }
    this.field = field;
    this.input = input;
  }

  public notBlank(): void {
    if (this.input.trim().length === 0) {
      throw MissingMandatoryValueException.forBlankValue(this.field);
    }
  }

  isValidUrl(): void {
    // See https://www.rfc-editor.org/rfc/rfc7230#section-3.2.6
    const isValid = validator.isURL(this.input, {
      protocols: ['http', 'https'],
      require_tld: true,
      require_protocol: true,
      require_host: true,
      allow_underscores: true,
      allow_trailing_dot: false,
      allow_protocol_relative_urls: false,
      allow_fragments: true,
      allow_query_components: true,
    });
    if (!isValid) {
      throw UnexpectedValueException.forExpectedFormat(this.field, this.input, 'url RFC-7230');
    }
  }
}
