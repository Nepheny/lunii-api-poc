import ShortUniqueId from 'short-unique-id';
import Assert from '../assertions/assert';

export default class UrlToCreate {
  public originalUrl: string;

  public shortUrl: string;

  constructor(originalUrl: string) {
    Assert.string('UrlToCreate.originalUrl', originalUrl).isValidUrl();
    const shortId = new ShortUniqueId({ length: 10 });
    this.originalUrl = originalUrl;
    this.shortUrl = shortId.randomUUID();
  }
}
