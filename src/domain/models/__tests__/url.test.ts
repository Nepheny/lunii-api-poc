import Url from '../url';
import { defaultUrlToCreate } from './url-to-create.test';
import UnexpectedValueException from '../../exceptions/unexpected-value.exception';
import MissingMandatoryValueException from '../../exceptions/missing-mandatory-value.exception';

export const defaultUrl = new Url(defaultUrlToCreate.originalUrl, defaultUrlToCreate.shortUrl);

describe('Url', () => {
  test('should build with minimal properties', () => {
    expect(defaultUrl).toBeInstanceOf(Url);
    expect(defaultUrl.originalUrl).toEqual(defaultUrlToCreate.originalUrl);
    expect(defaultUrl.shortUrl).toEqual(defaultUrlToCreate.shortUrl);
    expect(defaultUrl.nbClicks).toBeUndefined();
  });

  test('should build with optional nbClicks defined', () => {
    const url = new Url(defaultUrlToCreate.originalUrl, defaultUrlToCreate.shortUrl, 3);
    expect(url).toBeInstanceOf(Url);
    expect(url.originalUrl).toEqual(defaultUrlToCreate.originalUrl);
    expect(url.shortUrl).toEqual(defaultUrlToCreate.shortUrl);
    expect(url.nbClicks).toEqual(3);
  });

  test('should throw an error if originalUrl is not provided', () => {
    expect(() => new Url('', defaultUrlToCreate.shortUrl)).toThrow(UnexpectedValueException);
  });

  test('should throw an error if shortUrl is not provided', () => {
    expect(() => new Url(defaultUrlToCreate.originalUrl, '')).toThrow(MissingMandatoryValueException);
  });
});
