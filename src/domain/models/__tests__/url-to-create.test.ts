import UrlToCreate from '../url-to-create';
import UnexpectedValueException from '../../exceptions/unexpected-value.exception';

const defaultOriginalUrl = 'https://lunii.com';
export const defaultUrlToCreate = new UrlToCreate(defaultOriginalUrl);

describe('UrlToCreate', () => {
  test('should build', () => {
    expect(defaultUrlToCreate).toBeInstanceOf(UrlToCreate);
    expect(defaultUrlToCreate.originalUrl).toEqual(defaultOriginalUrl);
    expect(defaultUrlToCreate.shortUrl).toHaveLength(10);
  });

  test('should throw an error if originalUrl is not provided', () => {
    expect(() => new UrlToCreate('')).toThrow(UnexpectedValueException);
  });
});
