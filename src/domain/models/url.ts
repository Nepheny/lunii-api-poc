import Assert from '../assertions/assert';

export default class Url {
  constructor(
    public originalUrl: string,
    public shortUrl: string,
    public nbClicks?: number
  ) {
    Assert.string('Url.originalUrl', originalUrl).isValidUrl();
    Assert.string('Url.shortUrl', shortUrl).notBlank();
    if (nbClicks !== undefined) {
      Assert.number('Url.nbClicks', nbClicks).integer().min(0);
    }
  }
}
