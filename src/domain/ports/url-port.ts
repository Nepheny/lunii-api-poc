import Url from '../models/url';

export interface UrlPort {
  save(url: Url): Promise<void>;
  findByShortUrl(shortUrl: string): Promise<Url | null>;
  findAll(): Promise<Url[]>;
  updateNbVisited(shortUrl: string): Promise<void>;
}
