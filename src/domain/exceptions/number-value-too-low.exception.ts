import { AssertionException } from './assertion.exception';

export default class NumberValueTooLowException extends AssertionException {
  constructor(params: { field: string; minValue: number; value: number }) {
    super(params.field, `Value of field ${params.field} must be at least ${params.minValue} but was ${params.value}`);
  }
}
