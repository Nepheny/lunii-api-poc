import { InvalidValueException } from './abstract/invalid-value.exception';

export class AssertionException extends InvalidValueException {
  constructor(
    public readonly field: string,
    message: string
  ) {
    super(message);
  }
}
