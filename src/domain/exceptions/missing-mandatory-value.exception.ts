import { AssertionException } from './assertion.exception';

export default class MissingMandatoryValueException extends AssertionException {
  constructor(field: string, reason: string) {
    super(field, `${field} is mandatory but wasn't set (${reason})`);
  }

  public static forNullValue(field: string): MissingMandatoryValueException {
    return new MissingMandatoryValueException(field, 'null, undefined');
  }

  public static forBlankValue(field: string): MissingMandatoryValueException {
    return new MissingMandatoryValueException(field, 'blank');
  }
}
