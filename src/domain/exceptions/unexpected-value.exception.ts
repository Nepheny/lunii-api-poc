import { AssertionException } from './assertion.exception';

export default class UnexpectedValueException extends AssertionException {
  static forExpectedFormat(field: string, value: unknown, format: string): UnexpectedValueException {
    return new UnexpectedValueException(field, `${field} should have value in ${format} format but was '${value}'`);
  }
}
