import { BaseException } from './base.exception';

export abstract class InvalidValueException extends BaseException {}
