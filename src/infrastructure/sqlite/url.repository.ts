import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import { UrlPort } from '../../domain/ports/url-port';
import Url from '../../domain/models/url';

export default class UrlRepository implements UrlPort {
  private dbPromise = open({
    filename: './database.db',
    driver: sqlite3.Database,
  });

  async save(url: Url): Promise<void> {
    const db = await this.dbPromise;
    await db.run('INSERT INTO urls (originalUrl, shortUrl) VALUES (?, ?)', [url.originalUrl, url.shortUrl]);
  }

  async findByShortUrl(shortUrl: string): Promise<Url | null> {
    const db = await this.dbPromise;
    const row = await db.get('SELECT originalUrl, shortUrl, nbClicks FROM urls WHERE shortUrl = ?', [shortUrl]);
    if (row) {
      return new Url(row.originalUrl, row.shortUrl, row.nbClicks);
    }
    return null;
  }

  async findAll(): Promise<Url[]> {
    const db = await this.dbPromise;
    const rows = await db.all('SELECT originalUrl, shortUrl, nbClicks FROM urls');
    return rows.map((row) => new Url(row.originalUrl, row.shortUrl, row.nbClicks));
  }

  async updateNbVisited(shortUrl: string): Promise<void> {
    const db = await this.dbPromise;
    await db.run('UPDATE urls SET nbClicks = nbClicks + 1 WHERE shortUrl = ?', [shortUrl]);
  }
}
