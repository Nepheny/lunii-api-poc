import express, { NextFunction, Request, Response } from 'express';
import UrlAdapter from '../../sqlite/url.repository';
import UrlShorteningService from '../../../application/url-shortening.service';
import UrlToCreate from '../../../domain/models/url-to-create';

const urlAdapter = new UrlAdapter();
const urlShorteningService = new UrlShorteningService(urlAdapter);

const shorturlRouter = express.Router();

shorturlRouter.get('/analytics', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const urls = await urlShorteningService.getAll();
    res.status(200).json(urls);
  } catch (err) {
    next(err);
  }
});

shorturlRouter.get('/:shortUrl', async (req: Request, res: Response) => {
  const { shortUrl } = req.params;

  const url = await urlShorteningService.getOriginalUrl(shortUrl);
  if (url === null) {
    res.status(404).json({ error: 'URL not found' });
  } else {
    res.redirect(url.originalUrl);
  }
});

shorturlRouter.post('/', async (req: Request, res: Response, next: NextFunction) => {
  const { originalUrl } = req.body;
  if (!originalUrl) {
    res.status(400).json({ error: 'Original URL is required' });
  }

  try {
    const url = await urlShorteningService.saveShortUrl(new UrlToCreate(originalUrl));
    res.json(url);
  } catch (err) {
    next(err);
  }
});

export default shorturlRouter;
