import express from 'express';
import dotenv from 'dotenv';
import { initDB } from '../sqlite/database';
import { errorHandler } from './middlewares/error.handler';
import shorturlRouter from './controllers/shorturl.router';

dotenv.config();

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

// Middlewares
app.use('/api/shorturl', shorturlRouter);

// Error handling
app.use(errorHandler);

app.listen(port, async () => {
  await initDB();
  console.log(`Server is running at http://localhost:${port}`);
});
