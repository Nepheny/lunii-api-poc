# Lunii-api-poc

## Setup
- Le framework utilisé est express.
- La database est une base sqlite.
- Les tests unitaires sont écrits avec Jest.
- nodemon est installé pour permettre du hot-reload.
- eslint et prettier sont installés pour améliorer la qualité du code.

## Requirements
- node 22

## Quick start
Avant le premier lancement de l'environnement local de développement :
```bash
cp .env.SAMPLE .env
```
Pour lancer l'environnement local de développement :
```bash
npm run dev
```
Pour lancer les outils de qualité de code :
```bash
npm run lint
```
Pour lancer les tests unitaires :
```bash
npm run test
```

## Architecture
Le PoC est organisé selon les principes de l'architecture hexagonale afin de permettre une meilleure maintenabilité du code. L'arborescence est représentée ci-dessous (certains fichiers ne sont pas représentés par soucis de lisibilité) :
```bash
url-shortener/
├── src/
│   ├── application/
│   │   └── url-shortening.service.ts
│   ├── domain/
│   │   ├── assertions/
│   │   ├── exceptions/
│   │   │   └── abstract/
│   │   ├── models/
│   │   │   ├── __tests__/
│   │   │   ├── url.ts
│   │   │   └── url-to-create.ts
│   │   └── ports/
│   ├── infrastructure/
│   │   ├── express/
│   │   │   ├── controllers/
│   │   │   ├── middlewares/
│   │   │   └── server.ts
│   │   └── sqlite/
│   │       ├── database.ts
│   │       └── url.repository.ts
│   └── index.ts
├── package.json
└── README.md
```


## Axes d'améliorations
- Compléter les tests unitaires
- Ajouter des domain services pour permettre la gestion des erreurs à ce niveau.
- Ajouter des Dto pour éviter l'appel des objets du domaine dans la couche infrastructure.
- Ajouter des tests d'architecture hexagonale.
- Ajouter des tests e2e avec cucumber.js.
- Ajouter un ORM / query-builder.
- Ajouter un wrapper pour supprimer le code dupliqué relatif à la couche infrastructure.
- Améliorer la gestion des erreurs dans la couche infrastructure.
